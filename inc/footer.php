</div> <!-- div fermante de la classe "page-header-->

<div class="space-50"></div>
<div><a id="cRetour" class="cInvisible" href="#haut"></a></div>
<style>

</style>
<script>
document.addEventListener('DOMContentLoaded', function() {
  window.onscroll = function(ev) {
    document.getElementById("cRetour").className = (window.pageYOffset > 1000) ? "cVisible" : "cInvisible";
  };
});
</script>
<div class="footer1 footer-default" id="footer">
    <div class="btncookie" id="boxcookie"> <!-- bannière cookie -->
        <p class="p-cookie">Nous utilisons des cookies pour vous garantir une meilleure expérience et améliorer la performance de notre site.</p>
        <p>Cours Digital Network certifie de ne pas uiliser vos données personelles (blablabla)</p>     
        <button id="nocookie" type="button" class="btn btn-outline-info">Non</button>
        <button id="okcookie" type="button" class="btn btn-outline-info">Oui</button>   
    </div>
</div>

<div>
    <h2>Remerciements</h2>
    <p>Merci à l'institut Marie-Thérèse Solacroup pour tout le contenu mis à disposition, offrant des vidéos et des cours d'une valeur intellectuelle inestimable</p>
</div> 
<footer class="footer">
    <div class="container">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="https://maxlefort35.gitlab.io/portfolio-ml/">&copy; Maxime Lefort</a>
                </li>
                <li>
                    <a href="https://tourat_kevin.gitlab.io/portfolio-io/" >&copy; Kévin Tourat</a> 
                </li>        
                <li>
                    <a href="https://www.institutsolacroup.com/">&copy; Institut Marie-Thérèse Solacroup</a>
                </li>
                <li>
                    <a href="#">RGPD</a>
                </li> 
            </ul>
        </nav>
    </div>
</footer> 
    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script> -->
    <!-- <script src="./assets/js/core/popper.min.js" type="text/javascript"></script> -->
    <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script> 
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
</body>
</html>