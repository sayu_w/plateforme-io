<nav class="navbar navbar-img navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="15" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand yes-mobile" href="acceuil.php">
            <img src="image/logo-pc1" alt="logo-pc" class="img nav-gif no-mobile">           
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="dropdown nav-item ">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
                        <i class="material-icons">folder</i>Mes cours
                    </a>
                    <div class="dropdown-menu dropdown-with-icons"  style="width: 500px;">
                        <!-- liste des thèmes et cours -->
                        <div class="container">
                            <div class="row">             
                                <div class="col-sm">
                   
                                     <!-- 1 -->
                                    <a href="html5.php" class="dropdown-item">
                                        <i class="material-icons">filter_1</i>HTML 5
                                    </a>
                
                                    <a href="css-sass.php" class="dropdown-item">
                                        <i class="material-icons">filter_2</i>CSS / Sass
                                    </a>

                                    <a href="javascript.php" class="dropdown-item">
                                        <i class="material-icons">filter_3</i>JavaScript
                                    </a>

                                    <a href="wordpress.php" class="dropdown-item">
                                        <i class="material-icons">filter_4</i>Wordpress
                                    </a>
                                </div>
                                <div class="col-sm">
                                    
                                    <!-- 2 -->

                                    <a href="git.php" class="dropdown-item">
                                        <i class="material-icons">filter_5</i>Git
                                    </a>

                                    <a href="python.php" class="dropdown-item">
                                        <i class="material-icons">filter_6</i>Python
                                    </a>

                                    <a href="php.php" class="dropdown-item">
                                        <i class="material-icons">filter_7</i>PHP
                                    </a>

                                    <a href="symfony.php" class="dropdown-item">
                                        <i class="material-icons">filter_8</i>Symfony
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                    <!-- tuto -->
                <li class="dropdown nav-item ">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
                        <i class="material-icons">emoji_objects</i>Tuto
                    </a>
                    <div class="dropdown-menu dropdown-with-icons"  style="width: 300px;">
                        <div class="container">
                            <div class="row">                          
                                <div class="col-sm">

                                    <!-- liste tuto -->
                                    <a href="tuto_css.php" class="dropdown-item">
                                        <i class="material-icons">filter_1</i>Tuto en cours..
                                    </a>

                                    <!-- next here -->

                                </div>                                                  
                            </div>
                        </div>
                    </div>
                </li> 
                <li class="nav-item dropdown">
                    <!-- liste menu -->
                    <li class="nav-item">
                        <a class="nav-link" href="videos.php">
                            <i class="material-icons">video_call</i>Vidéos
                        </a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="actu.php">  
                            <i class="material-icons">fiber_new</i>Actus
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">
                            <i class="material-icons">chat</i>Contact
                        </a>
                </li>
                </li>
                    <!-- liste link réseaux sociaux -->
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://twitter.com/">
                        <i class="fa fa-twitter"></i>             
                    </a>            
                </li>
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://gitlab.com/">
                        <i class="fa fa-gitlab"></i>
                    </a>           
                </li>
                <li class="nav-item">
                    <a target="_blank" rel="noopener" class="nav-link" href="https://www.linkedin.com/">
                        <i class="fa fa-linkedin"></i>
                    </a>           
                </li>
                <li class="nav-item">
                    <a id="deco" class="nav-link" href="logout.php">
                    <i class="material-icons">power_settings_new</i>
                    </a>           
                </li>
            </ul>        
        </div>
    </div>
</nav>

<header></header>
    <div class="page-header header-filter purple-filter" data-parallax="true">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div class="brand text-center">
                        <div class="header_mobile">
                            <h1 class="txt-entete"><b> Cours<b><i> Digital Network</i></h1>
                            <h3 class="txt-entete"><i>Retrouvez vos cours en ligne.</i></h3> <br>
                        </div>
                        <!-- ici mettre titre dans le header, img, txt, titre ect.. -->
                    </div>
                </div>
            </div>
        </div>
    </div>