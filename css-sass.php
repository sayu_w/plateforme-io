<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="space-70"></div> 
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Cour Css partie 1</h2>
                  <div class="card-body">
                    <div class="space-20"></div>
                    <p>Dans ce premier cours, vous y trouverez les bases du Css 3. Une introduction comment débuter. Comment faire appel à vos fichiers dans votre code HTML.</p>                                                                   
                    <div class="space-70"></div>
                  </div>
                </div>
              </div>                                    
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">
                    <img class="img" src="image/css3.png" alt="js svg">
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/CSS3/CSS-Partie1.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Cour Css et design adaptatif</h2>
                  <div class="card-body">
                    <p>Dans cette deuxième partie de cours, vous y trouverez des compléments d'informations sur le Css avec des ressources</p>
                    <div class="space-30"></div>   
                    <p>Une introduction comment débuter. Comment faire appel a vos fichiers dans votre code HTML.</p>
               
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>                            
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <div class="space-30"></div>
                    <img class="img" src="image/css3.png" alt="js svg">
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/CSS3/Cours responsive design/Cours CSS et design adaptatif.html" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="card">
            <h3>Petite Grilles CSS</h3>
            <p class="codepen" data-height="500" data-theme-id="light" data-default-tab="result" data-user="MiisterK" data-slug-hash="LYpoBeG" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Tuto Css">
              <span>See the Pen <a href="https://codepen.io/MiisterK/pen/LYpoBeG">
                    Tuto Css</a> by Kevin Tourat (<a href="https://codepen.io/MiisterK">@MiisterK</a>)
                    on <a href="https://codepen.io">CodePen</a>.
              </span>
            </p>
            <script async src="https://static.codepen.io/assets/embed/ei.js"></script>
          </div>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Cour Grilles CSS</h2>
                  <div class="space-50"></div>
                  <div class="card-body">
                    <p>Les 15, 16 et 17 juin 2017 se tenait le Wordcamp Europe à Paris,
                    Morten Rand-Hendriksen, instructeur chez Linkedin et Lynda.com, a présenté les Grid CSS.</p>
                    <div class="space-50"></div>  
                    <a href="cour-année19-20/corsaire/front-end/CSS3/Cours responsive design/Grille-css_wceu2017.rar" download="Grille-css_wceu2017.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>                                     
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    
                    <img class="img" src="image/css3.png" alt="js svg">
                    <div class="space-20"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/CSS3/Cours responsive design/wceu2017.html" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">                             
                  <div class="card-body">                   
                    <img class="img" src="./cour-année19-20/corsaire/front-end/Bootstrap/TinDog Completed Website/images/dog-img.jpg" alt="chien">
                    <div class="space-30"></div>
                    <p style="text-align: center !important;">Site de rencontre pour chien !</p>
                    <a href="cour-année19-20/corsaire/front-end/Bootstrap/bootstrap.rar" download="bootstrap.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-20"></div>
                  </div>
                </div>
              </div>                                     
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">                   
                    <img class="img" src="image/bootstrap4.jpg" alt="js svg">
                    <h2>TinDog Website</h2>
                    
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Bootstrap/TinDog Completed Website/index.html" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>
                  <h2>Flexbox !</h2>                       
                  <div class="card-body">                                                                
                    <p>Le module des boîtes flexibles, aussi appelé « flexbox », a été conçu comme un modèle de disposition unidimensionnel</p>
                    <p>C'est une méthode permettant de distribuer l'espace entre des objets d'une interface ainsi que de les aligner.</p>
                    <div class="space-50"></div>                   
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <div class="space-30"></div>
                    <img class="img" src="image/css3.png" alt="js svg">                             
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Flexbox/flexbox-cheat-sheet.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-30"></div>
          <section class="jumbotron">
            <h2>Travailler c'est bien ! Mais en s'amusant c'est mieux !</h2>
            <img class="img" src="image/not-hear-2687975_640.jpg" alt="froggy" style="border-radius:10px;">
            <div class="space-30"></div>
            <a target="_blank" rel="noopener" href="https://flexboxfroggy.com/#fr" style="color:#45bd63;">FLEXBOX FROGGY</a>
          </section>
          <section class="jumbotron">
          <div class="card text-center">
            <div class="space-30"></div>
            <div class="card-header">Cours Css : Header partie 1</div>
            <div class="card-body">
              <div class="embed-responsive embed-responsive-16by9 img" id="cours_css"></div>
              <div class="space-30"></div>                       
              <a href="cour-année19-20/corsaire/front-end/CSS3/Header-css.zip" download="Header-css.zip" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
            </div>
          </div>
          <div class="card text-center">
            <div class="space-30"></div>
            <div class="card-header">Cours Css : Header partie 2</div>
            <div class="card-body">
              <div class="embed-responsive embed-responsive-16by9 img" id="cours_css2"></div>
              <div class="space-30"></div>                       
            </div>
          </div>
          <div class="card text-center">
            <div class="space-30"></div>
            <div class="card-header">Cours Css : Header partie 3</div>
            <div class="card-body">
              <div class="embed-responsive embed-responsive-16by9 img" id="cours_css3"></div>
              <div class="space-30"></div>                       
            </div>
          </div>
        </section>
          <div class="space-30"></div>
          <h3 style="color: white !important;" >Prochainement, des cours sur les sujets suivant !</h3>
          <section>
            <div class="row">
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">
                    <img class="img" src="image/sass-nedir-blog.jpg" alt="js svg">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">
                    <img class="img" src="image/bulma-banner.png" alt="js svg">
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="row">
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">
                  <div class="card-body">
                    <img class="img" src="image/materialio-940x400.jpg" alt="js svg">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">
                  <div class="card-body">
                    <img class="img" src="image/materialzieqsd.png" alt="js svg">
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-70"></div>      
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>