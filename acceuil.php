<?php

	session_start();

	require('src/log.php');

	
	if(!empty($_POST['email']) && !empty($_POST['password'])){

		require('src/connect.php');

		// VARIABLES
		$email 			= htmlspecialchars($_POST['email']);
		$password		= htmlspecialchars($_POST['password']);

		// ADRESSE EMAIL SYNTAXE
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

			header('location: acceuil.php?error=1&message=Votre adresse email est invalide.');
			exit();

		}

		// CHIFFRAGE DU MOT DE PASSE
		$password = "aq1".sha1($password."123")."25";

		// EMAIL DEJA UTILISE
		$req = $db->prepare("SELECT count(*) as numberEmail FROM user WHERE email = ?");
		$req->execute(array($email));

		while($email_verification = $req->fetch()){
			if($email_verification['numberEmail'] != 1){
				header('location: acceuil.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}
		}

		// CONNEXION
		$req = $db->prepare("SELECT * FROM user WHERE email = ?");
		$req->execute(array($email));

		while($user = $req->fetch()){

			if($password == $user['password'] && $user['blocked'] == 0){  // BLOQUAGE USER -> && $user['blocked'] == 0

				$_SESSION['connect'] = 1;
				$_SESSION['email']   = $user['email'];

				if(isset($_POST['auto'])){
					setcookie('auth', $user['secret'], time() + 364*24*3600, '/', null, false, true);
				}

				header('location: acceuil.php?success=1');
				exit();

			}
			else {

				header('location: acceuil.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}

		}

	}

?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

  <div class="main main-raised ">
    <div class="container background_body">
      <div class="section text-center">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">

            <!-- Mes actus et acces cours 2019 -->
            
            <section>
<!-- LOGIN -->
              <div class="space-30"></div>
              <div id="div_login" class="jumbotron">
                <?php if(isset($_SESSION['connect'])){  ?>

                  <p class="display-2"><img src="image/logo-mobile1.png" alt="logo du site"> Bonjour ! <img src="image/logo-mobile1.png" alt="logo du site"></p>
                  <p class="display-4">Bienvenue sur votre espace de cours !</p>

                  <?php
                  
                  if(isset($_GET['success'])){

                    echo'<div class="alert success">Vous êtes maintenant connecté.</div>';	
                  } ?>
                
              </div>
<!-- FIN LOGIN -->
                                            
               <!-- cours gratuit en ligne Udemy et autres -->
              <h1>Cours gratuit sur Udemy !</h1>              
              <section class="jumbotron">
                
                <div class="row">                  
                  <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                                   
                        <img class="img" src="image/html.png" alt="js svg">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 ">
                    <div class="card">
                      <h2>Introduction au Développement Web</h2>
                      <div class="card-body">
                                             
                        
                        <p>Ce cours offre <b>une vue d'ensemble sur le développement Web.</b> On va tout bien expliquer depuis le début.
                        Comment fonctionne un site Web ?</p>
                        
                        <a target="_blank" rel="noopener" type="button" href="https://www.udemy.com/course/introduction-au-developpement-web/" class="btn btn-outline-info">Accéder au cours</a>
                        <div class="space-30"></div>                       
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <!-- cours gratuit en ligne 2 -->

              <div class="space-50"></div>
              <section class="jumbotron">
               <div class="row">
                 <div class="col-sm-6">
                   <div class="card">
                   <div class="space-50"></div> 
                    <h2>Découvrir Javascript en 30 minutes</h2>                   
                     <div class="card-body">            
                      <p>Le but de ce petit cours c’est de vous faire découvrir l’univers Javascript !
                      Si vous devez choisir d’apprendre qu’un seul et unique langage de programmation, je vous recommande vivement d’apprendre Javascript. 
                        Pourquoi? Dans ce cours, je vais vous montrer que c’est le langage le plus populaire.</p>                        
                     </div>
                   </div>
                 </div>                                
                  <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                        <div class="space-30"></div>            
                        <img class="img" src="image/javascript-736400_640.png" alt="js svg">
                        <div class="space-20"></div>
                        <a target="_blank" rel="noopener" type="button" href="https://www.udemy.com/course/quest-ce-que-javascript/" class="btn btn-outline-info">Accéder au cours</a>
                        <div class="space-30"></div>
                      </div>
                    </div>
                  </div>
                </section>

             <!-- cours gratuit en ligne 3 -->

             <div class="space-50"></div>
             <section class="jumbotron">
             <div class="row">                  
               <div class="col-sm-6">
                 <div class="card">                     
                   <div class="card-body">
                     <div class="space-30"></div>            
                     <img class="img" src="image/css3.png" alt="illustration de cours CSS 3">
                     <div class="space-20"></div>
                     <a target="_blank" rel="noopener" type="button" href="https://www.udemy.com/course/apprendre-a-creer-des-sites-professionnels-avec-html5-et-css/" class="btn btn-outline-info">Accéder au cours</a>                  
                   </div>
                 </div>
               </div>
               <div class="col-sm-6 ">
                 <div class="card">
                   <h2>Apprendre à créer des sites professionnels avec HTML5 et CSS</h2>                   
                   <div class="card-body">            
                     <h3>Description</h3>
                     <p>Vous avez toujours voulu créer le site de vos rêves ? Commencez par les bases grâce à ce cours gratuit ! Ce cours gratuit vous permettra de survoler quelques notions en HTML5 et CSS3, pour vous faire une idée du langage. </p>
                    </div>
                 </div>
               </div>
             </div>
           </section>

           <div class="space-50"></div>

           <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                <div class="space-50"></div>
                 <h2>Méthode express' : créer un site avec WordPress</h2>
                  <div class="card-body">
                   <p>Découvrez WordPress pour créer des sites internet</p>
                   <p>A toute personne souhaitant apprendre à créer des sites internet avec WordPress rapidement.
                    WordPress est utilisé par plus d'un quart des sites web au monde, maîtriser son interface d'administration et savoir gérer 
                    un site WordPress sont des compétences de plus en plus appréciées dans le monde professionnel. </p>
                  </div>
                </div>
              </div>
               <div class="col-sm-6">
                 <div class="card">
                   <div class="card-body">
                     <div class="space-30"></div>
                     <img class="img" src="image/wordpress-1810453_640.jpg" alt="image de wordpress">
                     <div class="space-20"></div>
                     <a type="button" href="https://www.udemy.com/course/methode-express-creer-un-site-avec-wordpress/" class="btn btn-outline-info">Accéder au cours</a>
                   </div>
                 </div>
               </div>
             </section>

             <div class="space-50"></div>
             <h3 class="txt-entete">MOOC</h3>

             <section class="jumbotron">
               <div class="row">
                 <div class="col-sm-6">
                    <div class="card">                     
                      <div class="card-body">
                        <div class="space-30"></div>            
                        <img class="img" src="image/ANSSI.jpg" alt="image de l'anssi">
                        <div class="space-20"></div>
                        <a type="button" href="https://www.udemy.com/course/methode-express-creer-un-site-avec-wordpress/" class="btn btn-outline-info">Accéder au cours</a>                    
                      </div>
                    </div>
                  </div>
              
                 <div class="col-sm-6">
                  <div class="card">
                   <h2>Bienvenue sur le MOOC <br> de l'ANSSI.</h2>
                   
                    <div class="card-body">
                     <h3>Description</h3>
                     <p>Vous y trouverez l’ensemble des informations pour vous initier à la cybersécurité, approfondir vos connaissances,
                        et ainsi agir efficacement sur la protection de vos outils numériques.</p>
                    </div>
                  </div>
                </div>                           
               </section>
               <div class="space-70"></div>
<!-- Identification -->
                  <?php } else {?>
                          <h2>S'identifier</h2>
                          <p>Vous devez être connecté pour accéder au contenu.</p>
                          <?php if(isset($_GET['error'])) {

                            if(isset($_GET['message'])) {
                              echo'<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
                            }

                          } ?>

                          <form method="post" action="acceuil.php">
                          
                            <input class="input_co" type="email" name="email" placeholder="Votre adresse email" required />
                            <input class="input_co" type="password" name="password" placeholder="Mot de passe" required />
                            <br>
                            <button class="btn btn-outline-info" type="submit">S'identifier</button>
                            <label style="color: blue !important;" id="checkarea"><input id="check" type="checkbox" name="auto" checked />Se souvenir de moi</label>

                          </form>
                        
                          <p>Première visite sur Cours Digital Network ? <a style="color: blue !important;" class="inscription" href="inscription.php">Inscrivez-vous</a>.</p>
                        
                      <?php } ?>
<!-- Fin identification -->
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- 5 div de la class main-raised -->
<?php include "inc/footer.php"; ?>