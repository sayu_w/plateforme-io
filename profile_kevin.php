<?php include "inc/header.php"; ?>
<div class="main main-raised ">
    <div class="container background_body">
        <div class="section text-center">
            <div class="row">
                <div class="col-md-10 ml-auto mr-auto">
                    <h1>Profil de Kévin !</h1>
                    <div class="space-30"></div>
                    <img class="profile" src="image/carte_kevin.PNG" alt="profil-kevin" style="border-radius: 10px;">
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" href="https://tourat_kevin.gitlab.io/portfolio-io/"></a>
                    <p>Mail : tourat.kevin@gmail.com</p>
                    <p>Tel : 06.71.04.14.64</p>
                    <div class="jumbotron">
                        <div class="space-30"></div>
                        <a target="_blank" rel="noopener" href="https://discord.gg/38XVnHU"><img class="svg" src="image/discord.png" alt="discord"></a>
                        <a target="_blank" rel="noopener" href="https://join.slack.com/t/plateforme-io/shared_invite/zt-enufvjqc-6~5PHexRbEPPUezbtougdg"><img class="svg" src="image/slack.png" alt="slack"></a>
                        <a target="_blank" rel="noopener" href="https://twitter.com/TouratKevin"><img class="svg" src="image/twitter.png" alt="twitter"></a>
                        <a target="_blank" rel="noopener" href="https://www.linkedin.com/in/k%C3%A9vin-tourat-8b0657195/"><img class="svg" src="image/linkedin.png" alt="linkedin"></a>
                        <a target="_blank" rel="noopener" href="https://www.facebook.com/kevin.panda.5473"><img class="svg" src="image/facebook.png" alt="facebook"></a>                  
                        <div class="space-30"></div>
                        <a role="button" class="btn btn-outline-info" href="contact.php">Retour au contact</a>
                        <div class="space-30"></div>
                    </div>
                    <div class="space-70"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>