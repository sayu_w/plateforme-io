arAnimaux = ['dauphin', 'chien', 'chat', 'baleine', 'Aya nakamura']
arQualite = ['méchant', 'cruel', 'islamiste', 'gentil', 'bienveillant', "à la voix d'\"or\""]

arTotems = []

function getElementAleatoire(tableau) {
    let indexAlea = Math.floor(Math.random() * tableau.length)
    return tableau[indexAlea]
}

while (arTotems.length < 10) {

    var animal = getElementAleatoire(arAnimaux) + " " + getElementAleatoire(arQualite)

    if (arTotems.includes(animal)) {
        console.log(animal + " existe déjà");
    } else {
        arTotems.push(animal)
        let elLI = document.createElement("li")
        elLI.innerHTML = animal
        document.getElementById("totems").appendChild(elLI)
    }
}

/* Méthode 1 qui génère potentiellement des doublons
for (i = 0; i < 10; i = i + 1) {
    let indexAlea1 = Math.floor(Math.random() * arAnimaux.length)
    let indexAlea2 = Math.floor(Math.random() * arQualite.length)

    var animal = arAnimaux[indexAlea1] + " " + arQualite[indexAlea2]

    let elLI = document.createElement("li")
    elLI.innerHTML = animal
    document.getElementById("totems").appendChild(elLI)

}
*/