/* Définition des classes */
class Livre {
  constructor(id, auteur, titre) {
    this.id = id;
    this.auteur = auteur;
    this.titre = titre;
  }

  toString() { return this.titre+', écrit par : '+this.auteur; }
  toTR() { return `<tr><td>${this.titre}</td><td>${this.auteur}</td></tr>`}
  afficherInfos() {console.log(this.toString());}
};

class Bibliotheque {
  constructor(nom = 'Nouvelle bibliothèque') {
    this.nom = nom;
    this.arLivres = [];
  }

  ajouterLivre(auteur, titre) {
    // Ajout d'un livre avec incrément automatique de l'ID
    if (typeof auteur == 'string' && typeof titre == 'string' 
        && auteur.length>0 && titre.length>0 ) {
          // Recherche d'une id dispo
          // Méthode 1 : max+1 
          // var id = Math.max(...maBiblio.arLivres.map(obj => obj.id)) + 1;
          // Méthode 2 : 1er id dispo
          var id = 0
          while (this.arLivres.map(obj => obj.id).indexOf(id)>=0) { id++; }

      this.arLivres.push(new Livre(id, auteur, titre));
      return true;
    } else {
      return false;
    }
  }

  supprimerLivreId(id) {
    // Suppression d'un livre avec incrément automatique de l'ID

    // Recherche de l'id du livre dans le tableau 
    var index = this.arLivres.map(obj => obj.id).indexOf(id);

    if (index>=0) {
      // Supprime un élément à l'indice index
      this.arLivres.splice(index,1);
      return true;
    } else {
      // index non trouvé
      return false;
    }
  }
  
  modifierLivreId(id, newAuteur, newTitre) {
    // Modification d'un livre en cherchant l'ID

    // Recherche de l'objet 
    var oLivre = this.arLivres.find(obj => obj.id == id);
    if (oLivre) {
      // Réaffecte le titre et l'auteur si ils ne sont pas modifiés
      oLivre.titre = newTitre?newTitre:oLivre.titre;
      // Equivalent avec un if (pour l'auteur):
      if (newAuteur) {oLivre.auteur = newAuteur;}

      return true;
    } else {
      // Identifiant de livre non trouvé dans la bibliothèque
      return false;
    }
  }

  sauverLocal() {
    localStorage.setItem(this.nom, JSON.stringify(this));
  }

  chargerLocal() {
    var strBib = localStorage.getItem(this.nom)
    // Test de chargement
    if (strBib === null) {
      return false;
    } else {
      // Récupération des propriétés de l'objet
      var oBib = JSON.parse(strBib);
      this.nom = oBib.nom;
      this.arLivres = oBib.arLivres;
      return true;
    }
  }

  getTableHTML(bSuppress = false) {
    if (this.arLivres.length==0) {
      var el = document.createElement('p')
      el.innerHTML = 'Pas encore de livre dans la collection'
      return el;
    } else {
      var elTbl = document.createElement('table');
      var elHead = elTbl.createTHead();
      var colSuppr = bSuppress?'<td>Supprimer</td>':'';
      elHead.innerHTML = '  <tr><td>Id</td><td>Titre</td><td>Auteur</td>'+colSuppr+'</tr>\n';
      elTbl.appendChild(elHead) 
      var elBody = elTbl.createTBody();
      for (var l of this.arLivres) {
        var ligne = elBody.insertRow();
        ligne.insertCell().innerHTML = l.id;
        ligne.insertCell().innerHTML = l.titre;
        ligne.insertCell().innerHTML = l.auteur;
        if (bSuppress) {
          ligne.insertCell().innerHTML = `<a href="#" onclick="supprimer(${l.id});">✖ supprimer</a>`;  
        }
      };

      elTbl.appendChild(elBody)
      return elTbl;
    }
  }
};