<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Envoyer / Récupérer des données de formulaire</title>  
</head>

<body>

<?php
	// Affichage débug - Récupération des variables POSTées
	echo '<pre>$_POST vaut:';
	var_dump($_POST);
	echo '</pre>';
?>
<h1>Envoyer / Récupérer des données de formulaire</h1>
<form action="ex00_post_formulaire.php" method="post">
	<p><label for="maValeur">Entrer 1 valeur</label>
	<input type="text" name="maValeur" id="maValeur"></p>
	<input type="submit" value="Envoyer par POST">
</form>


<?php
	if(isset($_POST['maValeur'])){
        // Traitement prévu dans le programme
		$maValeur = $_POST['maValeur'];
		echo "<p><strong>Ma valeur vaut $maValeur </strong></p>";
	}
	else 
	{
        // Ici la page est chargée sans que les données du
        // formulaire aient été postées
		echo "<p>Ma valeur n'est pas définie</p>";
	}
?>

<p><a href="./">Dossier Parent</a></p>
</body>
</html>
