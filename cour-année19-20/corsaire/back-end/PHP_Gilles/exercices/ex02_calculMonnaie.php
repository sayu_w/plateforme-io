<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Calcul de monnaie</title>
	</head>
<body>
<h1>Calculateur de monnaie</h1>

<?php
	if(isset($_POST['prix']) && isset($_POST['paye'])){
		$prix = $_POST['prix'];
		$paye = $_POST['paye'];
	
		if($paye>=$prix){
			// Test si le montant fourni est suffisant
			$arendre = $paye - $prix;

			echo "<p>Monnaie à rendre: $arendre €</p><br/>";
			
			// Méthode plus générique

			// Définition du tableau des monnaies existantes
			$tabMonnaie = [20,10,5,2,1];
			// $tabMonnaie = [1];

			// Initialise un message d'info affiché à la fin
			$strInfo = "Il faut rendre <br/>";

			foreach($tabMonnaie as $monnaie) {
				// Arrêt de la boucle s'il n'y a plus rien à rendre
				if ($arendre == 0) { break; }
				
				$nb = floor($arendre / $monnaie);
				$arendre = $arendre % $monnaie;
				for($i=0; $i<$nb; $i++){ 
					/* Affichage des images pour le type de monnaie */
					echo "<img src='images/".$monnaie."euros.png' alt='$monnaie euros'/> ";
				}
				echo "<br/>";

				// Type de monnaie suivant le montant 
				$strType = $monnaie>2 ? "billet" : "pièce";
				$strType = $strType.($nb>1 ? "s":""); // gestion du pluriel
				
				// Mise à jour du message d'info
				$strInfo = $strInfo." - $nb $strType de $monnaie € <br/>";
			}
			
			// Affichage du message d'info après les images
			echo "<p><strong>$strInfo</strong></p>";

		}
		else{
			echo "<p><strong>Somme payée insuffisante!!</strong></p>";
		}
		
	} else {
		echo "<p>Saisir des montants et appuyer sur calculer.</p>";
	}
?>

<form method="post" action="ex02_calculMonnaie.php">
	<p><label for="prix">Entrez le prix de l'article : </label>
	<br/><input type="text" name="prix" id="prix" required /></p>
	<p><label for="paye">Entrez la somme pay&eacute;e en € :</label> 
	<br/><input type="text" name="paye" id="paye" required /></p>
	<p><input type="submit"/></p>
</form>

</body>
</html>
