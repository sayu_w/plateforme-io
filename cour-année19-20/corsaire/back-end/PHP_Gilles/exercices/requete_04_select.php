
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test de requêtes PHP</title>
</head>
<body>
    <h1>Utilisation de $_GET pour passer des arguments à la page</h1>
    <?php 
        echo "<pre>\$_GET vaut:";
        var_dump($_GET);
        echo "</pre>";

    
    // Traitement des actions $_GET
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == 'delete') {
                echo "<h2>Appel d'une requête pour supprimer la ligne ".$_GET['id']."</h2>";
                echo "<p>Todo</p>";
            } elseif ($_GET['action'] == 'modif') {
                echo "<h2>Appel d'une requête pour modifier la ligne ".$_GET['id']."</h2>";
                echo "<p>Todo</p>";
            }
        }
    ?>
    <h2>Affichage d'un table</h2>
    <?php 
        try { 
            /* Connexion à la BDD/BD/DB (database)
             * /!\ Attention à l'encodage avec le paramètre charset=utf8
             */
            $dbh = new PDO('mysql:host=localhost;dbname=cinema;charset=utf8', 'root', 'ROOT@mysql');

            // On fait une requête : film 
            // On récupére le FfilmID pour le réinjecter en argument de $_GET
            $query = 'SELECT Ftitre, Fannee, FilmID FROM film WHERE Fannee>=2000';
            $result = $dbh->query($query, PDO::FETCH_ASSOC);
            
            // Affichage des résultats de la requête dans une <table>
            echo "<p>Requête : $query</p>\n";
            if ($result) {
                // Affichage dfe la requête
                echo "<table>\n";
                echo"\t<tr><th>Titre</th><th>Année</th><th>Supprimer</th><th>Modifier</th></tr>\n";
                // Parcours des lignes de $result
                foreach($result as $row) {
                    echo "\t<tr><td>".$row['Ftitre']."</td><td>".
                    $row['Fannee']."</td><td><a href='requete_select_04.php?action=delete&id=".$row['FilmID']."'>Supprimer</a></td>".
                    "<td><a href='requete_select_04.php?action=modif&id=".$row['FilmID']."'>Modifier</a></td></tr>\n";
                }
                echo "</table>\n";       
            } else {
                // Gestion du cas où la requête échoue
                echo "<p>→ La requête n'a pas renvoyé de résultat</p>"; 
            }

     ?>


    <?php
            // Ferme la connexion à la DB
            $dbh = null;

        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    ?>
</body>
</html>