
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test de requêtes AJAX</title>
</head>
<body>
    <h1>Création d'une requête Javascript asynchrone sur une BDD</h1>
    
    <h2>Affichage d'une table avec un formulaire de recherche</h2>
    form

    <?php 
        try { 
            /* Connexion à la BDD/BD/DB (database)
             * /!\ Attention à l'encodage avec le paramètre charset=utf8
             */
            $dbh = new PDO('mysql:host=localhost;dbname=cinema;charset=utf8', 'root', 'ROOT@mysql');

            
            // On fait une requête : film 
            $query = 'SELECT Ftitre, Fannee FROM film WHERE Fannee>=2000';
            $result = $dbh->query($query, PDO::FETCH_ASSOC);
            
            // Affichage des résultats de la requête dans une <table>
            echo "<p>Requête : $query</p>\n";
            if ($result) {
                // Affichage dfe la requête
                echo "<table>\n";
                echo"\t<tr><th>Titre</th><th>Année</th></tr>\n";
                // Parcours des lignes de $result
                foreach($result as $row) {
                    echo "\t<tr><td>".$row['Ftitre']."</td><td>".$row['Fannee']."</td></tr>\n";
                }
                echo "</table>\n";       
            } else {
                // Gestion du cas où la requête échoue
                echo "<p>→ La requête n'a pas renvoyé de résultat</p>"; 
            }

     ?>


    <?php
            // Ferme la connexion à la DB
            $dbh = null;

        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    ?>
</body>
</html>