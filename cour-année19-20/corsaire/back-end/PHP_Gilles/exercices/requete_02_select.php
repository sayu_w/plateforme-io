
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test de requêtes PHP</title>
</head>
<body>
    <h1>Requêtes connexions PHP/MySQL</h1>
    <h2>Connexion à la base + Requête test</h2>
    <?php 
        try { 
            /* Connexion à la BDD/BD/DB (database)
             * /!\ Attention à l'encodage avec le paramètre charset=utf8
             */
            $dbh = new PDO('mysql:host=localhost;dbname=cinema;charset=utf8', 'root', 'ROOT@mysql');

            // On fait une requête : film 
            // l'argument PDO::FETCH_ASSOC permet de récupérer
            // uniquement le résultat sous forme de tableau associatif 
            // 
            $query = 'SELECT Ftitre, Fannee FROM film WHERE Fannee>=2000';
            $result = $dbh->query($query, PDO::FETCH_ASSOC);
            
            // Affichage des résultats de la requête dans une <table>
            echo "<p>Requête : $query</p>";
            echo "<table>\n<tr><th>Titre</th><th>Année</th></tr>";
            foreach($result as $row) {
                echo "<tr><td>".$row['Ftitre']."</td><td>".$row['Fannee']."</td></tr>\n";
            }
            echo "</table>\n";       
     ?>

    <h2>Requête associative : films + réalisateurs</h2>
    <?php 
    // SELECT * FROM table1, table2 WHERE table1.id=table2.id
        $query = 'SELECT Ftitre, CONCAT(Aprenom, \' \', Anom) as nom FROM artiste, film WHERE FrealisateurID=ArtisteID';
        $result = $dbh->query($query, PDO::FETCH_ASSOC);
        
        // Affichage des résultats de la requête dans une <table>
        echo "<p>Requête : $query</p>";
        echo "<table>\n<tr><th>Titre</th><th>Réalisateur</th></tr>";
        foreach($result as $row) {
            echo "<tr><td>".$row['Ftitre']."</td><td>".$row['nom']."</td></tr>\n";
        }
        echo "</table>\n";
     ?>

    <h2>Requête avec jointure (sur plusieurs tables)</h2>
    <?php 
        $query = 'SELECT Ftitre, CONCAT(Aprenom, \' \', Anom) as nom, Jrole '.
        'FROM film, joue, artiste '.
        'WHERE JFilmID = FilmID '.
        'AND JArtisteID = ArtisteID;';
        $result = $dbh->query($query, PDO::FETCH_ASSOC);

        // Affichage des résultats de la requête dans une <table>
        echo "<p>Requête : $query</p>";
        if ($result) {
            echo "<table>\n<tr><th>Titre</th><th>Artiste</th><th>Rôle</th></tr>";
            foreach($result as $row) {
                echo "<tr><td>".$row['Ftitre']."</td><td>".$row['nom']."</td><td>".$row['Jrole']."</td></tr>\n";
            }
            
            echo "</table>\n";    
        } else {
            echo "<p>→ La requête n'a pas renvoyée de résultat</p>";
        }

     ?>

    <h2>Requête imbriquée : Nombre de films par acteur</h2>
    <?php 
        $query = 'SELECT CONCAT(Aprenom, \' \', Anom) as nom, '.
        '(SELECT COUNT(*) as nb FROM joue WHERE ArtisteID=JArtisteID) AS nb_films '.
        'FROM artiste ';
        $result = $dbh->query($query, PDO::FETCH_ASSOC);

        // Affichage des résultats de la requête dans une <table>
        echo "<p>Requête : $query</p>";
        if ($result) {
            echo "<table>\n<tr><th>Artiste</th><th>Nb films</th></tr>";
            foreach($result as $row) {
                echo "<tr><td>".$row['nom']."</td><td>".$row['nb_films']."</td></tr>\n";
            }
            echo "</table>\n";    
        } else {
            echo "<p>→ La requête n'a pas renvoyée de résultat</p>";
        }

     ?>


    <?php
            // Ferme la connexion à la DB
            $dbh = null;

        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    ?>
</body>
</html>