<?php

    function connection_db() {
        // Cette fonction renvoie une connection vers la base de données
        // via l'objet PDO 
        $dbh = new PDO('mysql:host=localhost;dbname=chat;charset=utf8mb4',
        'root', 'ROOT@mysql', 
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, 
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
        return $dbh;
    }

?>