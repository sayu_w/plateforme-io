<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>


<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<p>Information sur le film:</p>

<?php
	
if(isset($_POST['filmid'])){
	$filmid = $_POST['filmid'];
	
	// connexion à la base de donnée	
	try{ // try permet de "surveiller" les erreurs
		$bdd = new PDO('mysql:host=pedago.uhb.fr; dbname=Base-ben_m_3; charset=utf8', 'ben_m', 'lNeODX7iyI4IJXBH1bW5');
	}
	catch (Exception $e){ // catch permet "d'attraper" les erreurs
		die('Erreur : '.$e->getMessage()); /* die arrête le programme en affichant un message d'erreur */
	}
	
	// requête pour extraire les infos du film
	$requete = $bdd->prepare("SELECT Ftitre, Fduree, Fannee, Fgenre, FrealisateurID FROM film WHERE FilmID=?");
	$requete->execute(array($filmid));
	
	// récupération des résultats
	$resultat = $requete->fetchall();
	foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
		$titre = $ligne['Ftitre'];
		$duree = $ligne['Fduree'];
		$heure = (int)($duree/60);
		$minute = $duree%60;
		$annee = $ligne['Fannee'];
		$genre = $ligne['Fgenre'];
		echo "<p><b>Titre</b> : ".$titre."</p>";
		echo "<p><b>Durée</b> : ".$heure."h".$minute."</p>";
		echo "<p><b>Année</b> : ".$annee."</p>";
		echo "<p><b>Genre</b> : ".$genre."</p>";
		
		$realisateurID = $ligne['FrealisateurID'];
		
		// requête pour extraire les infos du réalisateur
		$requete = $bdd->prepare("SELECT Aprenom, Anom FROM artiste WHERE ArtisteID=?");
		$requete->execute(array($realisateurID));
		
		// récupération des résultats
		$resultat = $requete->fetchall();
		foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
			$prenom = $ligne['Aprenom'];
			$nom = $ligne['Anom'];
			echo "<p><b>Réalisateur</b> : ".$prenom." ".$nom."</p>";
		}
		
	}
	
	// requête pour extraire les infos des acteurs
	$requete = $bdd->prepare("SELECT Aprenom, Anom, Jrole FROM joue JOIN artiste ON JartisteID=ArtisteID WHERE JfilmID=?");
	$requete->execute(array($filmid));
	
	echo "<p><b>Acteurs</b> : ";
	// récupération des résultats
	$resultat = $requete->fetchall();
	foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
		$prenom = $ligne['Aprenom' ];
		$nom = $ligne['Anom'];
		$role = $ligne['Jrole'];
		
		echo $prenom." ".$nom." (".$role."), ";
	}
	echo "</p>";
	
	// requête pour extraire la note moyenne du film
	$requete = $bdd->prepare("SELECT AVG(Nnote) AS Moyenne FROM note WHERE NFilmID=?");
	$requete->execute(array($filmid));
	// récupération des résultats
	$resultat = $requete->fetchall();
	foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
		$moyenne = $ligne['Moyenne'];
		echo "<p><b>Note moyenne</b> : ".$moyenne."/20</p>";
	}
}
else{
	echo "<p>Aucun film sélectionné</p>";
}

?>


</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>


<?php include("footer.php"); ?>

</body>
</html>
