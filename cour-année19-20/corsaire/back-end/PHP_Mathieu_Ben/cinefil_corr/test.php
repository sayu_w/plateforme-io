<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>


<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<p>Page "Test"</p>

<?php /* Ceci est un commentaire PHP */
	echo "<h3>Aujourd'hui nous sommes le ".date('d/m/Y')."</h3>"; /* Noter le point virgule
*/
?>

<?php echo '<h1>Test diapo 11</h1>'; ?>

<h2>Première étape : initialisation</h2>

<?php $phrase='Mathieu';
	echo "<p><em>Ici j'affecte la chaîne 'Mathieu' à une variable </em></p>";?>

<h2>Deuxième étape : ajout</h2>

<?php $phrase='Bonjour '.$phrase;
	echo "<p><em>Ici j'ajoute la chaîne 'Bonjour ' à ma
	variable initiale </em></p>";?>

<h2>Troisième étape : affichage</h2>

<?php echo "<p><em>Au final, ma variable vaut : $phrase </em></p>";?>

<?php echo "<h1>Test diapo 14</h1>";
	$mavariable=2;
	echo "<p>Contenu 1 de ma variable : $mavariable</p>";
	$mavariable= "Ceci est une chaîne";
	echo "<p>Contenu 2 de ma variable : $mavariable</p>";
?>

<?php $x=5; /*ligne à retirer dans le 2ème fichier*/
	echo "<h1>Test diapo 16</h1>";
	if(isset($x)){ /* si la variable $x existe */
		echo "<p><em>Ici la variable x existe et vaut $x</em></p>";
	}
	else{ /* sinon */
		echo "<p><em>Ici la variable x n'existe pas</em></p>";
	}
?>

<?php echo '<h1>Test diapo 29</h1>';
	$prixHT = 15.50;
	$prixTTC = $prixHT + $prixHT*19.6/100;
	echo '<p>Prix HT : $prixHT &euro;\nPrix TTC : $prixTTC &euro;</p>';
?>
<?php echo '<h1>Test diapo 30</h1>';
	$prixHT = 15.50;
	$prixTTC = $prixHT + $prixHT*19.6/100;
	echo "<p>Prix HT : $prixHT &euro;\nPrix TTC : $prixTTC &euro;</p>";
?>

<?php echo '<h1>Test diapo 31</h1>';
	$prixHT = 15.50;
	$prixTTC = $prixHT + $prixHT*19.6/100;
	echo 'Le prix TTC est de '.$prixTTC.' &euro;';
?>

<?php
	echo '<p>Contenu de $_SERVER["HTTP_HOST"] : <b>'.$_SERVER["HTTP_HOST"].'</b></p>';
	echo '<p>Contenu de $_SERVER["REQUEST_URI"] : <b>'.$_SERVER["REQUEST_URI"].'</b></p>';
?>


</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>


<?php include("footer.php"); ?>

</body>
</html>
