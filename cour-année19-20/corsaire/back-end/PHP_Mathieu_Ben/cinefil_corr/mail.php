<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>

<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<?php
// test de la bonne réception des données du formulaire
if(isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['comment']) && isset($_POST['sexe']) && isset($_POST['situation'])){
	echo "<p>Votre commentaire a bien été reçu. <br/>En vous remerciant pour votre participation !<p>";

	// Création de noms abrégés pour les variables récupérées du formulaire
	$pseudo = $_POST['pseudo'];
	$to = $_POST['mail'];
	$comment = $_POST['comment'];
	$sexe = $_POST['sexe'];
	$situation = $_POST['situation'];

	if(isset($_POST['send'])){
	echo "<p>Un email de confirmation va vous être envoyé dans quelques minutes.</p>";
	// Intitialisation de quelques informations statiques
	$subject = "Votre commentaire sur le site \"Cinéfil\"";
	$from = "webmaster@cinefil.com";

	// Corps du message
	$body = "Bonjour,\n".
		"Vous avez déposé un commentaire sur le site \"Cinéfil\" et nous vous en remercions\n\n".
		"Vous pouvez à tout moment nous demander de retirer ce commentaire en nous écrivant à webmaster@cinefil.com\n".
		"En espérant vous revoir très bientôt sur notre site!\n\n".
		"----------------------------\n".
		"Récapitulatif :\n".
		"Votre pseudo : ".$pseudo."\n".
		"Votre mail :".$to."\n".
		"Vous êtes : ".$sexe."\n".
		"Votre situation".$situation."\n".
		"Votre commentaire :\n".$comment."\n";
			
	// ajout du champ From: dans l'entête du message
	$header = "From:".$from;

	mail($to, $subject, $body, $header); //envoi du message
	}
	else{
	/* METTRE ICI LE CODE PERMETTANT D'AFFICHER LE RECAPITULATIF SUR LA PAGE mail.php */
		echo "<p><strong>Récapitulatif:</strong></p>";
		echo "Votre pseudo : $pseudo <br/>";
		echo "Votre mail : $to <br/>";
		echo "Vous êtes : $sexe <br/>";
		echo "Votre situation : $situation <br/>";
		echo "Votre commentaire :<br/>$comment <br/>";
	}
}
else{
	print "<p>Votre commentaire n'a pas été reçu correctement.<br/>Vous pouvez tenter de le soumettre à nouveau en retournant sur le site.<p>";
}
?>



<?php
	if(isset($_POST['pseudo'])){
		$pseudo = $_POST['pseudo'];
		echo "<p>Bonjour $pseudo !</p>";
	}

?>

<?php
	if(isset($_POST['url'])){
		$url = $_POST['url'];
		echo '<p><a href="'.$url.'">Retour au site</a></p>';
	}
	else{
		echo '<p><a href="index.php">Retour au site</a></p>';
	}
?>


</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>

<?php include("footer.php"); ?>

</body>
</html>