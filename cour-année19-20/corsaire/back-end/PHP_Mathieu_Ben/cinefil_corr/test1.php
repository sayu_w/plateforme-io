<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>


<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<p>Page "Test1"</p>

<?php 
	echo "<h1>Test diapo 16</h1>";
	if(isset($x)){ /* si la variable $x existe */
		echo "<p><em>Ici la variable x existe et vaut $x</em></p>";
	}
	else{ /* sinon */
		echo "<p><em>Ici la variable x n'existe pas</em></p>";
	}
?>

<?php
	if(isset($_POST['quantite']) && isset($_POST['type']) && isset($_POST['prixHT'])){
		$quantite = $_POST['quantite'];
		$type = $_POST['type'];
		$prixHT = $_POST['prixHT'];
	
		/* METTRE LE PROGRAMME DE TRAITEMENT ICI */
		$TVA = 0;
		if($type == "1"){
			$TVA = 19.6;
		}
		elseif($type == "2"){
			$TVA = 7;
		}
		elseif($type == "3"){
			$TVA = 5.5;
		}
		elseif($type == "4"){
			$TVA = 2.1;
		}
		
		$prixTTC = (($TVA/100)*$prixHT+$prixHT)*$quantite;
		
		echo "<p><strong>Le prix TTC de $quantite produits de type $type (TVA à $TVA%) ayant un prix hors taxe unitaire de $prixHT € est : $prixTTC €.</strong></p>";
	}
?>

<form method="post" action="test1.php">
	<p><label for="quantite">Entrez la quantité de produits : </label><br/><input type="text" name="quantite" id="quantite" /></p>
	<p><label for="type">Entrez le type de produit (1:TVA 19.6%; 2:TVA 7%; 3:TVA 5.5%; 4:TVA 2.1%) :</label> <br/><input type="text" name="type" id="type" /></p>
	<p><label for="prixHT">Entrez le prix unitaire hors taxe du produit :</label> <br/><input type="text" name="prixHT" id="prixHT" /></p>
	<p><input type="submit"/></p>
</form>

</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>

<?php include("footer.php"); ?>

</body>
</html>
