
<div id="back_to_top"> <!-- bloc pour l'image de la flêche de retour vers le haut de page -->
<!-- RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LE FICHIER monstyle.css -->

<!-- INSERER ICI L'IMAGE FlecheHaut.gif QUI SE TROUVE DANS LE DOSSIER images/boutons/ ET LA DIMENSIONNER EN 50px*50px -->
<a href="#top"><img id="fleche" title="Flèche vers le haut" src="images/boutons/FlecheHaut.gif" alt="Flèche vers le haut"/></a>
<!-- CREER UNE LIEN CLIQUABLE SUR L'IMAGE PERMETTANT DE RETOURNER EN HAUT DE LA PAGE  -->
</div>



<!-- début du bloc de pied de page -->
<footer>

<?php
	$url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
?>

<form action="mail.php" method="post">
	<p>
        <label for="pseudo">Votre pseudo :</label><br/>
        <input type="text" name="pseudo" id="pseudo" placeholder="Ex : Zozor" size="30" maxlength="10" />
    </p>
	<p>
        <label for="mail">Votre mail :</label><br/>
        <input type="email" name="mail" id="mail" placeholder="Ex : zozor@server.fr" size="30" maxlength="30" />
    </p>
	<p>
	<label for="comment">Votre commentaire :</label><br/>
	<textarea name="comment" id="comment" rows="10" cols="50" placeholder="Entrez votre commentaire">
       
	</textarea>
	</p>
	<p>
       Veuillez indiquer votre sexe :<br />
       <input type="radio" name="sexe" value="femme" id="femme" checked /> <label for="femme">Femme</label><br />
       <input type="radio" name="sexe" value="homme" id="homme" /> <label for="homme">Homme</label><br />
   </p>
   <p>
       <label for="situation">Indiquez votre situation</label><br />
       <select name="situation" id="situation">
           <option value="etudiant">Etudiant Rennes 2</option>
           <option value="personnel">Personnel Rennes 2</option>
           <option value="extérieur">Extérieur</option>
       </select>
   </p>
	<p>
       <input type="checkbox" name="send" id="send" /> <label for="send">Je souhaite recevoir un e-mail récapitulatif</label>
   </p>
	<p>
		<input type="submit" value="Valider"/>
	</p>
	<?php
		echo '<input type="hidden" name="url" value="'.$url.'" />';
	?>
</form>
<div id="copyright">
Copyright &copy; 2015 Mathieu Ben, Université de Rennes 2 <!-- AJOUTER APRES LE MOT "Copyright" LE CODE HMTL DU SYMBOLE "c commercial" -->
</div>
</footer>
<!-- fin du bloc de pied de page -->
