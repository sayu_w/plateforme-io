<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container">
    <div class="section text-center">
      <div class="space-110"></div>
        <h2 class="title">Documentation vidéos</h2>
        <!-- ici on met les vidéos appeler en JS par les id "iframe#" pages destiné au tuto de l'imts -->
        <h4>Pages 1</h4>
        <div class="jumbotron">
          <!-- <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe1"></div>
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe2"></div>
          </div> -->
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe3"></div>
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe4"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe5"></div>           
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe6"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe7"></div>            
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe8"></div>
          </div>
          <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe9"></div>           
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframe10"></div>
          </div>
        </div>
        <div class="space-30"></div> 
          <a type="button" class="btn btn-outline-info btn-lg btn-hidden"><i class="material-icons">skip_previous</i> Pages</a>
          <a href="videos2.php" type="button" class="btn btn-outline-info btn-lg">Pages<i class="material-icons">skip_next</i></a>
        <div class="space-30"></div>
      </div>
    </div>
  </div>
</div>

<?php include "inc/footer.php"; ?>