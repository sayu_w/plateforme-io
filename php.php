<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <h1>PHP et Base de données</h1>
          <div class="space-50"></div>                
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <h2>Galerie de photo en php</h2>
                  <div class="card-body">
                    <p>Document fourni sans images ! Mettez les vôtres dans les dossiers approprier pour que cela fonctionne corectement.</p>
                    <div class="space-50"></div>
                    <a href="cour-année19-20/corsaire/back-end/PHP_Gilles/galerie-php.rar" download="galerie-php.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <div class="space-30"></div>
                    <img class="img" src="image/galerie-php.png" alt="">
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Création Chat php</h2>                       
                  <div class="card-body">                          
                    <div class="space-30"></div>
                    <p>Contient:</p>                                 
                    <li>chat.sql (base de données)</li>
                    <li>config.php</li>
                    <li>lister.php</li>
                    <li>poster.php</li>
                    <li>index.html</li>
                    <p>A noter, pour la lecture de ces fichiers un serveur sera nécessaire (Wamp ou autre) puis l'ulisation du PhpMyAdmin !</p>
                    <div class="space-50"></div>
                    <a href="cour-année19-20/corsaire/back-end/PHP_Gilles/chat-ajax/chat1.zip" download="chat1.zip" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                   
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">
                    <div class="space-50"></div>            
                    <img class="img" src="image/php.jpg" alt="js svg">
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/back-end/PHP_Gilles/chat-ajax/index.html" class="btn btn-outline-info">Voir le chat</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>   
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>
                  <h2>Créer un utilisateur</h2>                       
                  <div class="card-body">                           
                    <div class="space-50"></div>
                    <p>Contient:</p>
                    <p>Mise a jours des fichiers</p>                         
                    <li>users.sql (base de données)</li>
                    <li>config.php</li>
                    <li>creer-user.php</li>                                                                               
                    <div class="space-50"></div>                      
                  </div>
                </div>
              </div>                                     
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">                             
                    <img class="img" src="image/php.jpg" alt="js svg">
                    <div class="space-20"></div>
                    <a href="cour-année19-20/corsaire/back-end/PHP_Gilles/chat-ajax-connexion/user-chat.rar" download="user-chat.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>                                             
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>   
          <section class="jumbotron">
            <div class="card text-center">
            <div class="space-30"></div>
              <div class="card-header">Exercices !
                <a target="_blank" rel="noopener" href="cour-année19-20/corsaire/back-end/PHP_Gilles/Exercices PHP.pdf" >Accéder au cours</a>
              </div>
              <div class="card-body">
                <img src="image/laptop-2592624_640.jpg" alt="" class="img">
                <p class="card-text"> 14 exercices php et html corigés ! Bonne lecture !</p>
                <a href="cour-année19-20/corsaire/back-end/PHP_Gilles/exercices/exo-corigé.rar" download="exo-corigé.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
              </div>               
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">                
            <div class="card">
              <h3 style="text-align: center !important;">Téléchargez cette base de données pour les cours et exercices suivants</h3>
              <a style="font-size: large; color:darkorange" href="cour-année19-20/corsaire/back-end/PHP_Mathieu_Ben/base_cinephile_MySQL_utf8.sql" download="base_cinephile_MySQL_utf8.sql">base_cinephile_MySQL_utf8.sql</a>
              <div class="space-20"></div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>
                  <h2>Cinefil</h2>                       
                  <div class="card-body">                                                          
                    <p>Contient:</p>                                                      
                    <li>fichiers css</li>
                    <li>fichiers images</li>
                    <li>fichiers polices</li>
                    <li>index.html</li>
                    <li>user.ini</li>
                    <li>htaccess</li>                                                                                  
                    <div class="space-30"></div>
                    <a href="cour-année19-20/corsaire/back-end/PHP_Mathieu_Ben/cinefil/cinefil.rar" download="cinefil.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                        
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>
                  <h2>Cinefil : corriger</h2>                       
                  <div class="card-body">
                    <div class="space-20"></div>
                    <p>Contient:</p>
                    <p>Mise a jours des fichiers</p>                         
                    <li>+ 10 fichiers php</li>
                    <p>index, header, footer, membres, mail, etc...</p>                                                                                               
                    <div class="space-50"></div>
                    <a href="cour-année19-20/corsaire/back-end/PHP_Gilles/chat-ajax-connexion/user-chat.rar" download="user-chat.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>
                  <h2>Cours complet PHP</h2>                       
                  <div class="card-body">  
                    <img src="image/php-mat.PNG" alt="cours" class="img">
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/back-end/PHP_Mathieu_Ben/PHP.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                          
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-20"></div>                                             
                  <div class="card-body">                               
                    <div class="space-20"></div>
                    <img src="image/php.jpg" alt="php" class="img">                                                                                  
                    <div class="space-20"></div>
                    <p style="text-align: center !important;">Exercices</p>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/back-end/PHP_Mathieu_Ben/TD_Formulaires.pdf" class="btn btn-outline-info">Formulaires</a>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/back-end/PHP_Mathieu_Ben/TD_PHP_MySQL.pdf" class="btn btn-outline-info">interface PHP</a>
                    <div class="space-30"></div>                                  
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>                         
                  <h2>Cours BD relationnelles</h2>                       
                  <div class="card-body">
                    <p>On s’intéressera aux données numériques, ce qui impose avant une phase de
                          numérisation ou digitalisation de la donnée, qui peut lui faire perdre de
                          l’information (précision).
                          Les données numériques sur lesquelles on va travailler se présentent sous
                          différentes formes.
                    </p>
                    <p>Des tableaux de valeurs (csv, excel, …)</p>
                    <li>des séries temporelles</li>
                    <li>des textes libres non structurés</li>
                    <li>des images, non structurées</li>                                 
                    <div class="space-50"></div>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                            
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">
                    <div class="space-30"></div>            
                    <img class="img" src="image/phpmyadmin.jpg" alt="image">                                  
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" href="cour-année19-20/corsaire/back-end/Base De Données/Cours BD relationnelles.pdf" class="btn btn-outline-info"> Accéder au cours</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <h2>Pratique de MySQL <br> et de PHP</h2>                       
                  <div class="card-body">
                    <div class="space-30"></div>                                    
                    <p>Ce livre présente l’utilisation de MySQL et de PHP pour la production et l’exploitation de sites web s’appuyant sur une base de données. 
                        Son principal objectif est d’exposer de la manière la plus claire et la plus précise possible les techniques
                        utilisées pour la création de sites web interactifs avec MySQL/PHP
                          
                    </p>                                   
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="Pratique_de_MySQL_et_PHP.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                            
              <div class="col-sm-6">
                <div class="card"> 
                  <h2>Développer un site web <br> en PHP et MySQL.</h2>                          
                  <div class="card-body">
                    <div class="space-20"></div>
                    <p>Ce livre détail très complet, reprend une explication détaillé de l'ensemble des langages Web en passant par le Javascript, html et css mais plus particulièrement le PHP et MySQL !</p>                                  
                    <div class="space-70"></div>
                    <a target="_blank" rel="noopener" type="button" href="Développer_un_site_web_en_PHP,_MySQL_et_Javascript.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-20"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="card text-center ">
            <div class="space-30"></div>             
              <div class="card-header">Sonic Pi: coder en Music avec Sonic Pi !</div>
              <div class="card-body">
                <img class="img" src="image/sonicpi_1.png" alt="sonic pi">
                <div class="space-30"></div>
                <a target="_blank" rel="noopener" href="cour-année19-20/corsaire/back-end/Music Coding/sonic-pi-intro.html" class="btn btn-outline-info">Allez a la page</a>
              </div>                      
            </div>
          </section>
          <div class="space-70"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>