<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">         
          <h1>JavaScript</h1>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Ensemble de cours et exercices javascript</h2>
                  <div class="card-body">
                    <div class="space-30"></div>
                    <p>Démarrage prise en main</p>
                    <p>Nous allons voir comment lancer et utiliser Javascript avec quelques pages d’exemples</p>
                    <p>Regroupe tout les exercices dans un fichier ZIP.</p>
                    <div class="space-50"></div>
                    <a href="cour-année19-20/corsaire/front-end/Javascript/js-cours.rar" download="js-cours.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <div class="space-20"></div>
                    <img class="img" src="image/javascript-736400_640.png" alt="js svg">
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Javascript/Cours et exercices de javascript.html" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>
            </div> 
          </section>
          
          <div class="card">
            <h3>Tuto : comment afficher l'heure en Javascript</h3>
            <p class="codepen" data-height="500" data-theme-id="light" data-default-tab="js,result" data-user="MiisterK" data-slug-hash="dyYBGOR" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Afficher l'heure en JS">
              <span>See the Pen <a href="https://codepen.io/MiisterK/pen/dyYBGOR">
                  Afficher l'heure en JS</a> by Kevin Tourat (<a href="https://codepen.io/MiisterK">@MiisterK</a>)
                  on <a href="https://codepen.io">CodePen</a>.
              </span>
            </p>
            <script async src="https://static.codepen.io/assets/embed/ei.js"></script>
          </div>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>Projet API</h2>             
                  <div class="card-body">                         
                    <h2>PDF descriptif </h2>
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="Projet API web.pdf" class="btn btn-outline-info">Accéder au cours</a>                     
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>                                   
              <div class="col-sm-6">
                <div class="card">                                            
                  <div class="card-body">
                              
                    <img class="img" src="image/javascript-736400_640.png" alt="js svg">
                    <div class="space-20"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Javascript/ex_1_appel_API.html" class="btn btn-outline-info">Exemple API</a>
                    <div class="space-20"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-30"></div>
                  <h2>1ère application en JS</h2>             
                  <div class="card-body">                         
                    <div class="space-20"></div>
                    <p>L’objectif de ce projet est de construire une première application en HTML et
                          Javascript côté client. L’application demandée est de type gestionnaire de
                          bibliothèque... (voir la suite)
                    </p>
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Javascript/S44 - Premiere appli JS/S44 - Premiere appli JS.zip" download="Premiere appli JS.zip" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="Projet_Application_JS.pdf" class="btn btn-outline-info">Accéder au cours</a>                      
                    <div class="space-30"></div>
                  </div>
                </div>
              </div>                                  
              <div class="col-sm-6">
                <div class="card">                                              
                  <div class="card-body">                                      
                    <img class="img" src="image/javascript-736400_640.png" alt="js svg">
                    <div class="space-20"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/Javascript/S44 - Premiere appli JS/LibrairieApp/index.html" class="btn btn-outline-info">Exemple Appli</a>
                    <div class="space-20"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <h2 style="color: white !important;">Framework JS : Sujet à suivre dans les prochains cours</h2>
          <section>
            <div class="row">
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">                          
                    <a target="_blank" rel="noopener" href="./cour-année19-20/corsaire/front-end/Javascript/book.html"><img class="img" src="image/node-js.png" alt="js svg"></a>             
                  </div>
                </div>
              </div>                                    
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">                                    
                    <img class="img" src="image/vu_js.jpg" alt="js svg">                                              
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="row">
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">                                       
                    <img class="img" src="image/angularjpg.jpg" alt="js svg">                                                 
                  </div>
                </div>
              </div>                                     
              <div class="col-sm-6">
                <div class="card" style="background-color: transparent !important; border: none !important; box-shadow: none;">                     
                  <div class="card-body">                                       
                    <img class="img" src="image/React.png" alt="js svg">                                                     
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-70"></div>
        </div>
      </div>
    </div>
  </div>
</div>
 <!-- end  -->
<?php include "inc/footer.php"; ?>