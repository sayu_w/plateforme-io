<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>
   
<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          
            <!-- formulaire temporaire à effet visuel uniquement, a changer avec des concordences php -->
           

          <p>Contactez nous par : <br> Slack, Discord ou Gmail</p>          
          <div class="container jumbotron">
            <div class="space-30"></div>
            <a rel="tooltip" data-placement="bottom" data-original-title="Voir le profil" href="profile_max.php"><img class="svg-two" src="image/profile-max.png" alt="kevin"></a>
            <a rel="tooltip" data-placement="bottom" data-original-title="Voir le profil" href="profile_kevin.php"><img class="space-footer svg-two" src="image/profile_kevin-black.png" alt="kevin"></a>
            <div class="space-30"></div>
            <a target="_blank" rel="noopener" href="https://discord.gg/38XVnHU"><img class="svg" src="image/discord.png" alt="discord"></a>
            <a target="_blank" rel="noopener" href="https://join.slack.com/t/plateforme-io/shared_invite/zt-enufvjqc-6~5PHexRbEPPUezbtougdg"><img class="svg" src="image/slack.png" alt="slack"></a>
              <!-- <a href=""><img class="space-footer svg" src="image/email.png" alt="gmail"></a> -->
            <div class="space-30"></div>
          </div>
          <div class="space-70"></div>        
        </div>
      </div>
    </div>
  </div>
</div>
<?php include "inc/footer.php"; ?>